<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Author Detail Page</title>
</head>
<body>

	<jsp:include page="header.jsp"/>

	<h1>${blogAuthor.userName}details:</h1>
	<br /> ${blogAuthor.userDetails.firstName}
	<br /> ${blogAuthor.userDetails.lastName}
	<br /> ${blogAuthor.userDetails.email}
	<br /> ${blogAuthor.userDetails.street}
	<br /> ${blogAuthor.userDetails.hoseNr}
	<br /> ${blogAuthor.userDetails.ZIP}
	<br /> ${blogAuthor.userDetails.city}
	<br /> ${blogAuthor.userDetails.country}
	<br />

	
</body>
</html>