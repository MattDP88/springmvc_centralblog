<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update author</title>
</head>
<body>
<h1>Update author ${blogAuthor.userName}:</h1>
${Error}
<form method="POST">
First name: <input type="text" name="firstName" value="${blogAuthor.userDetails.firstName}"/><br/>
Last name: <input type="text" name="lastName" value="${blogAuthor.userDetails.lastName}"/><br/>
E-mail: <input type="text" name="email" value="${blogAuthor.userDetails.email}"/><br/>
Street: <input type="text" name="street" value="${blogAuthor.userDetails.street}"/><br/>
House number: <input type="text" name="hoseNr" value="${blogAuthor.userDetails.hoseNr}"/><br/>
Zip: <input type="text" name="ZIP" value="${blogAuthor.userDetails.ZIP}"/><br/>
City: <input type="text" name="city" value="${blogAuthor.userDetails.city}"/><br/>
Country: <input type="text" name="country" value="${blogAuthor.userDetails.country}"/><br/>
<input type="submit" name="Submit"/>
</form>
</body>
</html>