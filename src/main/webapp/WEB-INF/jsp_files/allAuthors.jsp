<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Find User</title>
</head>
<body>

<c:forEach items="${listAuthors}" var="author">
		<form action="findAuthor" method="post">
			<input type="text" hidden="true" name="userName" value="${author.userName}"/>
			<input type="submit" value="${author.userName}" />
		</form>
</c:forEach>

</body>
</html>