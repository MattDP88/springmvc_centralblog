<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="findAuthor">
		<input type="submit" value="Find user" />
	</form>
	<form action="findAll">
		<input type="submit" value="Find All" />
	</form>
		<form action="logOut">
		<input type="submit" value="logOut" />
	</form>
	<c:if test="${blogAuthor.userName==loggedInAuthor}">
		<form action="updateAuthor">
			<input type="text" name="blogAuthor" value="${blogAuthor.userName}"
				hidden="true" /> <input type="submit" value="Update" />

		</form>
		<form action="updatePassword">
			<input type="text" name="blogAuthor" value="${blogAuthor.userName}"
				hidden="true" /> <input type="submit" value="Update password" />
		</form>
		<form action="deleteAuthor">
			<input type="text" name="blogAuthor" value="${blogAuthor.userName}"
				hidden="true" /> <input type="submit" value="Delete profile" />

		</form>
	</c:if>
</body>
</html>