package be.intecbrussel.springmvc.services.interfaces;

import mongoclasses.beans.BlogAuthor;


public interface UpdateAuthor {
	public boolean updateAuthor(BlogAuthor author);

}
