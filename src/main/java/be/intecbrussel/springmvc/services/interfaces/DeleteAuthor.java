package be.intecbrussel.springmvc.services.interfaces;

import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

public interface DeleteAuthor {

	public boolean deleteAuthor(BlogAuthor blogauthor, String password);
}
