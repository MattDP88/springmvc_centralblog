package be.intecbrussel.springmvc.services.interfaces;

import org.springframework.stereotype.Service;

import mongoclasses.beans.BlogAuthor;


public interface CreateAuthor {

	public boolean createAuthor(BlogAuthor author);
}
