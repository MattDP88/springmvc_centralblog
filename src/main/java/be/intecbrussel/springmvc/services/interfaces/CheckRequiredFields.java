package be.intecbrussel.springmvc.services.interfaces;


import org.springframework.stereotype.Service;

import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

public interface CheckRequiredFields {
	public boolean checkAuthor(BlogAuthor author);
	public boolean checkUserDetails(UserDetails userdet);

}
