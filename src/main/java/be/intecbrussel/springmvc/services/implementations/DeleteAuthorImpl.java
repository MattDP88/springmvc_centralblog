package be.intecbrussel.springmvc.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import be.intecbrussel.springmvc.services.interfaces.DeleteAuthor;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

@Service
@Lazy
public class DeleteAuthorImpl implements DeleteAuthor {
	
	@Autowired
	private SessionObject sessionObject;

	@Override
	public boolean deleteAuthor(BlogAuthor blogAuthor, String password) {
		if (DAOFactory.getBlogAuthorDao().checkuserCredentials(blogAuthor.getUserName(), password)) {
			DAOFactory.getBlogAuthorDao().deleteAuthor(blogAuthor.getUserName());
			sessionObject.logOut();
			return true;
		}

		return false;
	}
}
