package be.intecbrussel.springmvc.services.implementations;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import be.intecbrussel.springmvc.services.interfaces.CreateAuthor;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import mongoclasses.beans.BlogAuthor;

@Service
public class CreateAuthorImpl implements CreateAuthor {

	@Override
	public boolean createAuthor(BlogAuthor author) {
		author.setTimeStamp(LocalDateTime.now());
		if (DAOFactory.getBlogAuthorDao().createAuthor(author)){
			return true;
		}
		return false;
	}

}
