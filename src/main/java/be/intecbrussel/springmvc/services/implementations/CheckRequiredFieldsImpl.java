package be.intecbrussel.springmvc.services.implementations;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import be.intecbrussel.springmvc.services.interfaces.CheckRequiredFields;
import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

@Service
public class CheckRequiredFieldsImpl implements CheckRequiredFields {

	@Override
	public boolean checkAuthor(BlogAuthor author) {
		if (StringUtils.isBlank(author.getUserName()) || StringUtils.isBlank(author.getPasswd()) || !this.checkUserDetails(author.getUserDetails())){
			return false;
		}
		return true;
	}

	@Override
	public boolean checkUserDetails(UserDetails userdet) {
		if (StringUtils.isBlank(userdet.getFirstName()) || StringUtils.isBlank(userdet.getLastName())){
			return false;
		}
		return true;
	}

}
