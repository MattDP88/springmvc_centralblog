package be.intecbrussel.springmvc.services.implementations;

import org.springframework.stereotype.Service;

import be.intecbrussel.springmvc.services.interfaces.UpdateAuthor;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import mongoclasses.beans.BlogAuthor;

@Service
public class UpdateAuthorImpl implements UpdateAuthor {

	@Override
	public boolean updateAuthor(BlogAuthor author) {
		if (DAOFactory.getBlogAuthorDao().updateAuthor(author)){
			return true;
		}
		return false;
	}
}
