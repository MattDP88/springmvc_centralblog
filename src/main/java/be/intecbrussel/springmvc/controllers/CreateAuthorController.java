package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import be.intecbrussel.springmvc.services.interfaces.CheckRequiredFields;
import be.intecbrussel.springmvc.services.interfaces.CreateAuthor;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

@Controller
@RequestMapping("createAuthor")
@Scope("request")
public class CreateAuthorController {
	
	@Autowired
	private SessionObject sessionObject;
	@Autowired
	private CheckRequiredFields check;
	@Autowired
	private CreateAuthor create;

	@RequestMapping(method = RequestMethod.GET)
	public String getForm() {
		return "newAuthor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(BlogAuthor auth, UserDetails userdet, ModelMap model) {
		auth.setUserDetails(userdet);
		if (check.checkAuthor(auth) && check.checkUserDetails(userdet)) {
			if (create.createAuthor(auth)){
				BlogAuthor authorFromDB = DAOFactory.getBlogAuthorDao().findAuthorbyId(auth.getUserName());
				sessionObject.logIn(authorFromDB);
				model.addAttribute("loggedInAuthor", sessionObject.getAuthor().getUserName());
				return "authorDetail";
			}
			model.addAttribute("Error", "A Person with this username already exists. Please try a different username");
			return "newAuthor";
		}
		model.addAttribute("Error", "Please make sure you fill in all required fields");
		return "newAuthor";

	}
}
