package be.intecbrussel.springmvc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

@Controller
@RequestMapping("findAll")
@Scope("request")
public class FindAllController {

	@Autowired
	private SessionObject sessionObject;

	@RequestMapping(method = RequestMethod.GET)
	public String getForm(ModelMap model) {
		if (sessionObject.isLoggedIn()) {
			List<BlogAuthor> requestedAuthors = DAOFactory.getBlogAuthorDao().findAll();
			if (!requestedAuthors.isEmpty()) {
				model.addAttribute("listAuthors", requestedAuthors);
				return "allAuthors";
			}
			model.addAttribute("Error", "No such username!");
			return "findAll";

		}
		return "noAccess";
	}

}
