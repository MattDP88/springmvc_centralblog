package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import be.intecbrussel.springmvc.services.implementations.UpdateAuthorImpl;
import be.intecbrussel.springmvc.services.interfaces.CheckRequiredFields;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

@Controller
@RequestMapping("updateAuthor")
@Scope("request")
public class UpdateAuthorController {

	@Autowired
	private SessionObject sessionObject;
	@Autowired
	private CheckRequiredFields check;
	@Autowired
	private UpdateAuthorImpl update;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getForm(@RequestParam (value="blogAuthor",required=false) String blogAuthor, ModelMap map) {
		if (sessionObject.isLoggedIn()) {
			
			if (sessionObject.getAuthor().getUserName().equalsIgnoreCase(blogAuthor))
				map.addAttribute("blogAuthor", sessionObject.getAuthor());
				return "updateAuthor";
		}
		return "noAccess";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(BlogAuthor auth, UserDetails userdet, ModelMap model) {
		auth.setUserName(sessionObject.getAuthor().getUserName());
		auth.setPasswd(sessionObject.getAuthor().getPasswd());
		auth.setUserDetails(userdet);
		if (check.checkUserDetails(userdet)) {
			if (update.updateAuthor(auth)) {
				BlogAuthor authorFromDB= DAOFactory.getBlogAuthorDao().findAuthorbyId(auth.getUserName());
				sessionObject.logIn(authorFromDB);
				model.addAttribute("loggedInAuthor", sessionObject.getAuthor().getUserName());
				return "authorDetail";
			}
			model.addAttribute("Error", "Can't update author.");
			return "updateAuthor";
		}
		model.addAttribute("Error", "Please make sure you fill in all required fields");
		return "updateAuthor";
	}
}
