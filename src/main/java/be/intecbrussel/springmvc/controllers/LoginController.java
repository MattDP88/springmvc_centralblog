package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

@Controller
@RequestMapping("login")
@Scope("request")
public class LoginController {
	
	@Autowired
	private SessionObject sessionObject;

	@RequestMapping(method = RequestMethod.GET)
	public String getForm(ModelMap model) {
		if (sessionObject.isLoggedIn()){
			model.addAttribute("loggedInAuthor", sessionObject.getAuthor().getUserName());
			model.addAttribute("blogAuthor", sessionObject.getAuthor());
			return "authorDetail";
		}
		return "login";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(BlogAuthor blogAuthor, ModelMap model) {
		if (DAOFactory.getBlogAuthorDao().checkuserCredentials(blogAuthor.getUserName(), blogAuthor.getPasswd())){
			BlogAuthor authorFromDB = DAOFactory.getBlogAuthorDao().findAuthorbyId(blogAuthor.getUserName());
			sessionObject.logIn(authorFromDB);
			model.addAttribute("loggedInAuthor", sessionObject.getAuthor().getUserName());
			model.addAttribute("blogAuthor", sessionObject.getAuthor());
			return "authorDetail";
		}
		model.addAttribute("Error", "Username and/or password were wrong. Please try again!");
		return "login";

	}
}
