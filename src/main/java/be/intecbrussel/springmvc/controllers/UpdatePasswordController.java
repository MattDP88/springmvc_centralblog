package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

@Controller
@RequestMapping("updatePassword")
@Scope("request")
public class UpdatePasswordController {
	
	@Autowired
	private SessionObject sessionObject;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getForm() {
		if (sessionObject.isLoggedIn()){
			return "updatePasswordSecurityCheck";
		}
		return "noAccess";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@RequestParam("newPasswd") String password, BlogAuthor auth, ModelMap model) {
		if (DAOFactory.getBlogAuthorDao().checkuserCredentials(auth.getUserName(), auth.getPasswd())){
			if (auth.getUserName().equalsIgnoreCase(sessionObject.getAuthor().getUserName())){
				auth.setPasswd(password);
				auth.setUserDetails(DAOFactory.getBlogAuthorDao().findAuthorbyId(auth.getUserName()).getUserDetails());
				if(DAOFactory.getBlogAuthorDao().updatePassword(auth)){
				BlogAuthor authorFromDB = DAOFactory.getBlogAuthorDao().findAuthorbyId(auth.getUserName());
				sessionObject.logIn(authorFromDB);
				model.addAttribute("blogAuthor", authorFromDB);
				model.addAttribute("loggedInAuthor", authorFromDB.getUserName());
				return "authorDetail";
				}
			}
			model.addAttribute("Error", "Something went wrong trying to update your password! Please try again later.");
			return "updatePasswordSecurityCheck";
		}
		model.addAttribute("Error", "Username and/or password are wrong. Please try again.");
		return "updatePasswordSecurityCheck";
			
		

	}

}
