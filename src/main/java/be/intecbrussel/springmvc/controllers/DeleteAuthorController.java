package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import be.intecbrussel.springmvc.services.interfaces.DeleteAuthor;
import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

@Controller
@RequestMapping("deleteAuthor")
@Scope("request")
@SessionAttributes("blogAuthor")
public class DeleteAuthorController {

	@Autowired
	private SessionObject sessionObject;
	@Autowired
	private DeleteAuthor delete;

	@RequestMapping(method = RequestMethod.GET)
	public String getForm(@RequestParam(value = "blogAuthor", required = false) String blogAuthor, ModelMap map) {
		if (sessionObject.isLoggedIn()) {
			if (sessionObject.getAuthor().getUserName().equalsIgnoreCase(blogAuthor))
				map.addAttribute("blogAuthor", sessionObject.getAuthor());
				return "deleteForm1";
		}
		return "noAccess";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String handleForm(@RequestParam(value = "passwd", required = false) String passwd, ModelMap map) {
		if (sessionObject.isLoggedIn()) {
			BlogAuthor blogAuthor = (BlogAuthor) map.get("blogAuthor");
			if (sessionObject.getAuthor().getUserName().equalsIgnoreCase(blogAuthor.getUserName())) {
				if (delete.deleteAuthor(blogAuthor, passwd)) {
					map.addAttribute("blogAuthor", new BlogAuthor());
					return "index";
				}
				map.addAttribute("Error", "Wrong password");
				map.addAttribute("blogAuthor", sessionObject.getAuthor());
				return "deleteForm1";
			}
			map.addAttribute("Error", "Something went wrong. Please try again later");
			map.addAttribute("blogAuthor", sessionObject.getAuthor());
			return "deleteForm1";
		}
		return "noAccess";
	}
}
