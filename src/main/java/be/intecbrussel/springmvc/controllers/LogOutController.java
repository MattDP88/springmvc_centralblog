package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import be.intecbrussel.springmvc.utilities.SessionObject;

@Controller
@RequestMapping("logOut")
@Scope("request")
public class LogOutController {

	
	@Autowired
	private SessionObject sessionObject;
	
	@RequestMapping(method=RequestMethod.GET)
	public String logOut(){
		sessionObject.logOut();
		return "index";
	}
	
}
