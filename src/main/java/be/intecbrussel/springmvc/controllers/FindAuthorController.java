package be.intecbrussel.springmvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import be.intecbrussel.springmvc.utilities.DAOFactory;
import be.intecbrussel.springmvc.utilities.SessionObject;
import mongoclasses.beans.BlogAuthor;

@Controller
@RequestMapping("findAuthor")
@Scope("request")
public class FindAuthorController {
	
	@Autowired
	private SessionObject sessionObject;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getForm() {
		if (sessionObject.isLoggedIn()){
			return "findUser";
		}
		return "noAccess";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(BlogAuthor auth, ModelMap model) {
		BlogAuthor requestedAuthor=DAOFactory.getBlogAuthorDao().findAuthorbyId(auth.getUserName());
		if (requestedAuthor!=null){
			model.addAttribute("blogAuthor", requestedAuthor);
			model.addAttribute("loggedInAuthor", sessionObject.getAuthor().getUserName());
			return "authorDetail";
		}
		model.addAttribute("Error", "No such username!");
		return "findUser";

	}
}


