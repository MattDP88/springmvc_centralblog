package be.intecbrussel.springmvc.utilities;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mongoclasses.beans.BlogAuthor;

@Component
@Scope ("session")
public class SessionObject {
	private boolean loggedIn;
	private BlogAuthor author;
	
	public SessionObject(){
		
	}
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public BlogAuthor getAuthor() {
		return author;
	}
	
	public synchronized void logIn(BlogAuthor author){
		this.loggedIn=true;
		this.author=author;
				
	}
	
	public synchronized void logOut(){
		this.loggedIn=false;
		this.author=null;
	}
	
	

}
