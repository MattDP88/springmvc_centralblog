package be.intecbrussel.springmvc.utilities;

import mongoclasses.dao.implementations.BlogAuthorMongoDaoImpl;
import mongoclasses.dao.interfaces.BlogAuthorDao;

public class DAOFactory {

	private volatile static BlogAuthorDao dao = new BlogAuthorMongoDaoImpl("springmvc");
	
	private DAOFactory(){
		
	}
	
	public static BlogAuthorDao getBlogAuthorDao(){
		return dao;
	}
}
