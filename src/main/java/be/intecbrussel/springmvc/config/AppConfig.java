package be.intecbrussel.springmvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import mongoclasses.beans.BlogAuthor;
import mongoclasses.beans.UserDetails;

@Configuration
@ComponentScan(basePackages="be.intecbrussel.")
public class AppConfig {

	@Bean
	@Scope("session")
	public UserDetails userDetails(){
		return new UserDetails();
	}
	
	@Bean
	@Scope("session")
	public BlogAuthor blogAuthor (UserDetails userDetails){
		return new BlogAuthor();
	}
}
