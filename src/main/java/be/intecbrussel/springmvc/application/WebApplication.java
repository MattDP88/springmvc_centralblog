package be.intecbrussel.springmvc.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages="be.intecbrussel.")
public class WebApplication {
	public static void main (String[] args) throws Throwable{
		SpringApplication.run(WebApplication.class, args);
		
	}
	

}
